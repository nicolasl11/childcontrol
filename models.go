package main

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Account struct {
	gorm.Model
	Username string `json:"username"`
	Password []byte `json:"password"`
}

type Child struct {
	gorm.Model
	Name      string    `json:"name"`
	BeginDate time.Time `json:"begin-date"`
	EndDate   time.Time `json:"end-date"`
}
