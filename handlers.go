package main

import (
	"encoding/json"
	"html/template"
	"net/http"
	"strconv"
	"time"
)

func handleRoot(w http.ResponseWriter, req *http.Request) {
	var (
		tmpl     *template.Template
		err      error
		children []Child
		now      = time.Now()
	)

	tmpl, err = template.ParseFiles("templates/index.gohtml")

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Error with webpage"))
		return
	}

	db.Where("end_date > ?", now).Find(&children)

	var dates = []slice{}

	for i := -1; i <= 12; i++ { // looop over each month in the range.
		var (
			searchMonth = now.AddDate(0, i, 0)
			date        = slice{searchMonth, []childData{}}
		)

		searchMonth = searchMonth.AddDate(0, 0, -(searchMonth.Day() - 1))

		for _, child := range children { // check for each month if what children are present in that month
			var (
				beginDate = child.BeginDate.AddDate(0, 0, -(child.BeginDate.Day() - 1))
				endDate   = child.EndDate.AddDate(0, 0, 0)
			)

			if beginDate.Before(searchMonth) && endDate.After(searchMonth) {
				var (
					color            = STAY
					byear, bmonth, _ = beginDate.Date()
					eyear, emonth, _ = endDate.Date()
				)

				if byear == searchMonth.Year() && bmonth == searchMonth.Month() {
					color = FIRST
				} else if eyear == searchMonth.Year() && emonth == searchMonth.Month() {
					color = LAST
				}

				date.Children = append(date.Children, childData{child, color})
			}
		}

		dates = append(dates, date)
	}

	tmpl.Execute(w, dates)
}

func handleList(w http.ResponseWriter, req *http.Request) {
	var tmpl, err = template.ParseFiles("templates/list.gohtml")

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Error with webpage"))
		return
	}

	var children []Child

	db.Find(&children)

	tmpl.Execute(w, children)
}

func handleUpdate(w http.ResponseWriter, req *http.Request) {
	var (
		tmpl *template.Template
		err  error

		id    = req.FormValue("id")
		child Child
	)

	tmpl, err = template.ParseFiles("templates/update.gohtml")

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Error with webpage"))
		return
	}

	db.Where(id).First(&child)

	tmpl.Execute(w, struct {
		Child     Child
		BeginDate string
		EndDate   string
	}{
		Child:     child,
		BeginDate: child.BeginDate.Format("2006-01-02"),
		EndDate:   child.EndDate.Format("2006-01-02"),
	})
}

func apiUpdate(w http.ResponseWriter, req *http.Request) {
	var (
		id        = req.FormValue("id")
		name      = req.FormValue("name")
		beginDate = req.FormValue("begin-date")
		endDate   = req.FormValue("end-date")
		child     Child
		beginTime time.Time
		endTime   time.Time
		js        []byte
		err       error
	)

	if id == "" || name == "" || beginDate == "" || endDate == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("One or more fields were not filled in"))
		return
	}

	beginTime, err = time.Parse("2006-01-02", beginDate)
	endTime, err = time.Parse("2006-01-02", endDate)

	db.Where(strconv.Atoi(id)).First(&child)

	db.Model(&child).Updates(Child{Name: name, BeginDate: beginTime, EndDate: endTime})

	js, err = json.Marshal(child)

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Error with webpage"))
		return
	}

	w.Write(js)
}

func apiLogin(w http.ResponseWriter, req *http.Request) {
	var (
		username   = req.FormValue("username")
		password   = req.FormValue("password")
		account    Account
		jsonResult []byte
	)

	if db.First(&account, Account{Username: username, Password: []byte(password)}).RecordNotFound() {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Wrong Username or Password"))
		return
	}

	jsonResult, err = json.Marshal(struct {
		Id   uint   `json:"id"`
		Name string `json:"username"`
	}{
		account.ID,
		account.Username,
	})

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Wrong Username or Password"))
		return
	}

	w.Write(jsonResult)
}

func apiAdd(w http.ResponseWriter, req *http.Request) {
	var (
		name      = req.FormValue("name")
		beginDate = req.FormValue("begin-date")
		endDate   = req.FormValue("end-date")
		beginTime time.Time
		endTime   time.Time
		js        []byte
		err       error
	)

	if name == "" || beginDate == "" || endDate == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("One or more fields were not filled in"))
		return
	}

	beginTime, err = time.Parse("2006-01-02", beginDate)
	endTime, err = time.Parse("2006-01-02", endDate)

	var child = Child{
		Name:      name,
		BeginDate: beginTime,
		EndDate:   endTime,
	}

	db.Save(&child)

	js, err = json.Marshal(child)

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Error with webpage"))
		return
	}

	w.Write(js)
}

func apiGetChildren(w http.ResponseWriter, req *http.Request) {
	var children []Child

	db.Find(&children)

	var json, err = json.Marshal(children)

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Error with webpage"))
		return
	}

	w.Write(json)
}

func apiCreateAccount(w http.ResponseWriter, req *http.Request) {
	var (
		username   = req.FormValue("username")
		password   = req.FormValue("password")
		account    Account
		jsonResult []byte
		err        error
	)

	if username == "" || password == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Invalid Username or Password"))
		return
	}

	if !db.First(&account, Account{Username: username, Password: []byte(password)}).RecordNotFound() {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("User already exists"))
		return
	}

	account = Account{
		Username: username,
		Password: []byte(password),
	}

	db.Save(&account)

	jsonResult, err = json.Marshal(struct {
		Id   uint   `json:"id"`
		Name string `json:"username"`
	}{
		account.ID,
		account.Username,
	})

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Invalid Username or Password"))
		return
	}

	w.Write(jsonResult)
}
