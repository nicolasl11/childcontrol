package main

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

const (
	LAST  = "red"
	STAY  = "black"
	FIRST = "green"
)

type childData struct {
	Child  Child
	Status string
}

type slice struct {
	Ttime    time.Time
	Children []childData
}

var (
	db  *gorm.DB
	err error
)

func databaseInit(dbfile string) *gorm.DB {
	var tdb, terr = gorm.Open("sqlite3", dbfile)

	if terr != nil {
		fmt.Printf("database init: %s", terr.Error())
	}

	tdb.CreateTable(&Account{})
	tdb.AutoMigrate(&Account{})

	tdb.CreateTable(&Child{})
	tdb.AutoMigrate(&Child{})

	return tdb
}

func toNL(m time.Month) string {
	switch m {
	case 1:
		return "Januari"
	case 2:
		return "Februari"
	case 3:
		return "Maart"
	case 4:
		return "April"
	case 5:
		return "Mei"
	case 6:
		return "Juni"
	case 7:
		return "Juli"
	case 8:
		return "Augustus"
	case 9:
		return "September"
	case 10:
		return "Oktober"
	case 11:
		return "November"
	case 12:
		return "December"
	}

	return ""
}
