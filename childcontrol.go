package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

var (
	// the multiplexing router for alle the url mapping
	router *mux.Router
)

func main() {
	router = mux.NewRouter()

	router.HandleFunc("/", handleRoot).Methods("GET")
	router.HandleFunc("/lijst", handleList).Methods("GET")
	router.HandleFunc("/update", handleUpdate).Methods("GET")

	router.HandleFunc("/login", apiLogin).Methods("POST")
	router.HandleFunc("/add", apiAdd).Methods("POST")
	router.HandleFunc("/getlist", apiGetChildren).Methods("GET")
	router.HandleFunc("/update", apiUpdate).Methods("POST")

	router.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))

	http.Handle("/", router)

	db = databaseInit("database.sqlite")

	//var hash = sha512.New()
	//hash.Write([]byte("annahs"))
	//db.Save(&Account{Username: "shanna", Password: hash.Sum(nil)})

	http.ListenAndServe(":8080", nil)

	db.Close()
}
