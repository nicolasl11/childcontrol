package main

import (
	"crypto/sha512"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
)

func TestLogin(t *testing.T) {
	db = databaseInit("test.sqlite")

	var logins = []struct {
		username string
		password string
		status   int
		body     string
	}{
		{"collerprise", "pass", http.StatusOK, "{\"id\":1,\"username\":\"collerprise\"}"},
		{"wrong", "thing", http.StatusUnauthorized, "Wrong Username or Password"},
	}

	var (
		hash = sha512.New()
	)

	hash.Write([]byte("pass"))
	db.Save(&Account{Username: "collerprise", Password: hash.Sum(nil)})

	for _, checkValues := range logins {
		var args = url.Values{}

		args.Set("username", checkValues.username)

		hash = sha512.New()
		hash.Write([]byte(checkValues.password))
		var encryptedPassword = hash.Sum(nil)
		args.Set("password", fmt.Sprintf("%s", encryptedPassword))

		var (
			req = httptest.NewRequest(
				"POST", "http://somewhere.nowhere", nil)
			w = httptest.NewRecorder()
		)

		req.PostForm = args

		apiLogin(w, req)

		if w.Code != checkValues.status {
			t.Errorf("Expected %d, got %d", w.Code, checkValues.status)
		}

		if w.Body.String() != checkValues.body {
			t.Errorf("Expected %s, got %s", checkValues.body, w.Body.String())
		}
	}

	db.Close()

	os.Remove("test.sqlite")
}

func TestAddChild(t *testing.T) {
	db = databaseInit("test.sqlite")

	var children = []struct {
		name      string
		beginDate string
		endDate   string
		status    int
		body      string
	}{
		{"mila", "2018-01-01", "2018-02-01", http.StatusOK, ""},
		{"", "", "", http.StatusUnauthorized, "One or more fields were not filled in"},
	}

	for _, checkValues := range children {
		var (
			req  = httptest.NewRequest("POST", "http://somewhere.nowhere", nil)
			w    = httptest.NewRecorder()
			args = url.Values{}
		)

		args.Set("name", checkValues.name)
		args.Set("beginDate", checkValues.beginDate)
		args.Set("beginDate", checkValues.endDate)
		req.Form = args

		apiAdd(w, req)

		if w.Code != checkValues.status {
			t.Errorf("Exepted %d, got %d", checkValues.status, w.Code)
		}

		if w.Body.String() != checkValues.body {
			t.Errorf("Exepted %s, got %s", checkValues.body, w.Body.String())
		}

	}

	db.Close()

	os.Remove("test.sqlite")
}

func TestCreateAccount(t *testing.T) {
	db = databaseInit("test.sqlite")

	var logins = []struct {
		username string
		password string
		status   int
		body     string
	}{
		{"collerprise", "pass", http.StatusOK, "{\"id\":1,\"username\":\"collerprise\"}"},
		{"", "", http.StatusUnauthorized, "Invalid Username or Password"},
		{"collerprise", "pass", http.StatusUnauthorized, "User already exists"},
	}

	for _, checkValues := range logins {
		var args = url.Values{}

		args.Set("username", checkValues.username)
		args.Set("password", checkValues.password)

		var (
			req = httptest.NewRequest("POST", "http://somewhere.nowhere", nil)
			w   = httptest.NewRecorder()
		)

		req.Form = args

		apiCreateAccount(w, req)

		if w.Code != checkValues.status {
			t.Errorf("Exepted %d, got %d", checkValues.status, w.Code)
		}

		if w.Body.String() != checkValues.body {
			t.Errorf("Expected %s, got %s", checkValues.body, w.Body.String())
		}
	}

	db.Close()

	os.Remove("test.sqlite")
}
