CREATE TABLE "accounts" (
	"id" integer primary key autoincrement,
	"created_at" datetime,
	"updated_at" datetime,
	"deleted_at" datetime,
	"username" varchar(255),
	"password" blob
);

INSERT INTO "accounts" VALUES(1,'2018-03-23 12:55:01.991059341+01:00','2018-03-23 12:55:01.991059341+01:00',NULL,'shanna',X'2FD8497B6B385460A8F239B715A578B0D80899F4FC39D24FF1048F3A2516CBF108DACB72DDC046319E93C84FD3BB0008AB1073EEA25B1A0BC20751AB0A66B8C3');
DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('accounts',1);
CREATE INDEX idx_accounts_deleted_at ON "accounts"(deleted_at);
